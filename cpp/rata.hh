#ifndef RATA_HH
#define RATA_HH

#include <vector>
#include <string>
class Rata
{
    public:
        Rata();
        ~Rata();

        void lisaaSuora(double pituus, bool vaihto);
        void lisaaMutka(double sade, double kulma, bool vaihto);

        void asetaNimi(std::string& radanNimi);

        double edellisenPalanPituus(unsigned int pieceIndex);

        double haluttuNopeus(double& etaisyys,
                             unsigned int pieceIndex,
                             double inPieceDistance);

        void setKaistoja(unsigned int maara)
        {
            kaistoja_ = maara;
        }

        // vasen kaista on nollas
        unsigned int tavoiteKaista(unsigned int pieceIndex);

    private:

        unsigned int seuraavaPala(unsigned int pieceIndex);
        unsigned int edellinenPala(unsigned int pieceIndex);

        double matkaaSeuraavaanMutkaan(unsigned int &pieceIndex);

        struct Pala
        {
            double pituus;
            double kulma;
            double sade;
            bool vaihto;
        };

        std::string radanNimi_;

        std::vector<Pala> rata_;

        unsigned int kaistoja_;

        unsigned int seurIndex_;
        Rata(const Rata& other);
        Rata& operator=(const Rata& other);
};

#endif // RATA_HH
