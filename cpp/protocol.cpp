#include "protocol.h"

namespace hwo_protocol
{

  jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data)
  {
    jsoncons::json r;
    r["msgType"] = msg_type;
    r["data"] = data;
    return r;
  }

  jsoncons::json make_join(const std::string& name, const std::string& key)
  {
    jsoncons::json data;
    data["name"] = name;
    data["key"] = key;
    std::cout << "Normal join" << std::endl;
    return make_request("join", data);
  }
  jsoncons::json make_join(const std::string& name, const std::string& key,
                           const std::string& race)
  {
      jsoncons::json data;
      jsoncons::json botId;

      botId["name"] = name;
      botId["key"] = key;

      data["botId"] = botId;
      data["trackName"] = race;
      data["password"] = "vboyz";
      data["carCount"] = 1;

      std::cout << name << " trying to join race: " << race << std::endl;
      return make_request("joinRace", data);
  }
  jsoncons::json make_ping()
  {
    return make_request("ping", jsoncons::null_type());
  }

  jsoncons::json make_turbo()
  {
      jsoncons::json data = "Koirat ulvoo, karavaani kulkee";
    return make_request("turbo", data);
  }

  jsoncons::json make_throttle(double throttle)
  {
    return make_request("throttle", throttle);
  }

    jsoncons::json make_switch(const std::string& direction)
    {
        return make_request("switchLane", direction);
    }

   jsoncons::json make_race(const std::string& name,
                            const std::string& key,
                            const std::string& rata)
   {
      jsoncons::json data;
      jsoncons::json botId;


      botId["name"] = name;
      botId["key"] = key;

      data["botId"] = botId;
      data["trackName"] = rata;
      data["password"] = "vboyz";
      data["carCount"] = 1;
      std::cout << name << " creating race: " << rata << std::endl;
      return make_request("createRace", data);

   }



}  // namespace hwo_protocol
