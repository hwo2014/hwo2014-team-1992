#include "rata.hh"

#include <iostream>
#include <math.h>

Rata::Rata():  rata_(), kaistoja_(0),seurIndex_(0)
{
    //ctor
}

Rata::~Rata()
{
    //dtor
}
void Rata::asetaNimi(std::string& radanNimi)
{
    radanNimi_ = radanNimi;
}

void Rata::lisaaSuora(double pituus, bool vaihto)
{
    rata_.push_back(Pala {pituus, 0.0, 0.0, vaihto});
}

void Rata::lisaaMutka(double sade, double kulma, bool vaihto)
{
    double pituus = (2*M_PI*abs(sade)/360)*abs(kulma);
    rata_.push_back(Pala {pituus, kulma, sade, vaihto});
}

double Rata::edellisenPalanPituus(unsigned int pieceIndex)
{
    return rata_.at(edellinenPala(pieceIndex)).pituus;
}

double Rata::haluttuNopeus(double& etaisyys,
                           unsigned int pieceIndex,
                           double inPieceDistance)
{


    //double etaisyys = tamanPalanEtaisyys;

    //std::cout << "matkaa seuraavaan mutkaan" << etaisyys << std::endl;
    //std::cout << "säde: " << rata_.at(pieceIndex).sade << std::endl;
    //std::cout << "kulma: " << rata_.at(pieceIndex).kulma << std::endl;

    //double nopeus = (abs(rata_.at(pieceIndex).sade)*etaisyys/10)/abs(rata_.at(pieceIndex).kulma)
    //                    + 4*abs(rata_.at(pieceIndex).sade)/abs(rata_.at(pieceIndex).kulma) ;

    //std::cout << "kulmavauhti: " <<  10*fabs(rata_.at(seurIndex_).sade/rata_.at(seurIndex_).kulma) << std::endl;

    double tamanPalanEtaisyys = rata_.at(pieceIndex).pituus - inPieceDistance;
    double kulmaNopeus = 0.0;
    double kulmaNopeus2 = 0.0;

    // seuraava mutka
    etaisyys = matkaaSeuraavaanMutkaan(pieceIndex) + tamanPalanEtaisyys;

    kulmaNopeus = 22*sqrt(fabs(rata_.at(seurIndex_).sade))
                  /fabs(rata_.at(seurIndex_).kulma);


    //sitä seuraava mutka
    double etaisyys2 = matkaaSeuraavaanMutkaan(pieceIndex) + tamanPalanEtaisyys;

    kulmaNopeus2 = 22*sqrt(fabs(rata_.at(seurIndex_).sade))
                   /fabs(rata_.at(seurIndex_).kulma);

    if(radanNimi_ == "france" || radanNimi_ == "usa")
    {
        kulmaNopeus *= 0.8;
        kulmaNopeus2 *= 0.8;
        //katsotaan myös seuraava mutka
    }
    if(etaisyys > 20)
    {
        double nopeus1 = 0.0;
        double nopeus2 = 0.0;

        if(radanNimi_ == "imola" || radanNimi_ == "england")
        {
            nopeus1 = kulmaNopeus + kulmaNopeus*etaisyys/400;

            //katsotaan myös seuraava mutka
            nopeus2 = kulmaNopeus2
                      + kulmaNopeus2*(etaisyys+etaisyys2)/400;
        }
        else
        {

            nopeus1 = kulmaNopeus + kulmaNopeus*pow(etaisyys/350, 1.05);
            nopeus2 = kulmaNopeus2 + kulmaNopeus2*pow((etaisyys+etaisyys2)/350,1.05);
        }
        //std::cout << "Nopeus1: " << nopeus1 << std::endl;
        //std::cout << "Nopeus2: " << nopeus2 << std::endl;
        if( etaisyys2 < 200)
        {

            if( nopeus2 < nopeus1 - 0.3)
            {
                etaisyys += etaisyys2;
                return nopeus2;
            }
            return nopeus1;
        }

        return nopeus1;
    }
    //OHJELMAN TÄRKEIN KOODIRIVI
    // arvio siitä mitä nopeutta pitäisi ajaa

    return kulmaNopeus;
}

unsigned int Rata::tavoiteKaista(unsigned int pieceIndex)
{
    double optimirata = 0;

    while(!rata_.at(pieceIndex).vaihto)
    {
        pieceIndex = seuraavaPala(pieceIndex);
    }
    pieceIndex = seuraavaPala(pieceIndex);

    while(!rata_.at(pieceIndex).vaihto)
    {
        optimirata += rata_.at(pieceIndex).sade*rata_.at(pieceIndex).kulma;
        //std::cout << "lasketaan" << pieceIndex << std::endl;
        pieceIndex = seuraavaPala(pieceIndex);
    }
    //std::cout << "suunta:" << optimirata << std::endl;
    if(optimirata < 0)
    {
        return 0;
    }

    return kaistoja_;
}

double Rata::matkaaSeuraavaanMutkaan(unsigned int &pieceIndex)
{
    double etaisyys = 0.0;
    //pieceIndex = seuraavaPala(pieceIndex);

    while((rata_.at(pieceIndex).kulma < 5 && rata_.at(pieceIndex).kulma > -5))
    {
        etaisyys += rata_.at(pieceIndex).pituus;
        pieceIndex = seuraavaPala(pieceIndex);
    }

    seurIndex_ = pieceIndex;
    return etaisyys;
}


unsigned int Rata::seuraavaPala(unsigned int pieceIndex)
{
    if(pieceIndex < rata_.size() - 1)
    {
        return ++pieceIndex;
    }
    return 0;
}


unsigned int Rata::edellinenPala(unsigned int pieceIndex)
{
    if(pieceIndex > 0)
    {
        return --pieceIndex;
    }
    return pieceIndex < rata_.size() - 1;
}
