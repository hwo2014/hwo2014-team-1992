#include "game_logic.h"
#include "protocol.h"

using namespace hwo_protocol;


double const KIIHDYTYS = 0.005;
//double const JARRUTUS = 0.2;

int const HIDASKULMA = 5;
int const NOPEAKULMA = 35;

//std::string const BOTNAME = "virtualBoys";

game_logic::game_logic(std::string name)
    : action_map
{
    { "join", &game_logic::on_join },
    { "gameInit", &game_logic::on_game_init },
    { "gameStart", &game_logic::on_game_start },
    { "carPositions", &game_logic::on_car_positions },
    { "turboAvailable", &game_logic::on_turbo_av },
    { "crash", &game_logic::on_crash },
    { "gameEnd", &game_logic::on_game_end },
    { "error", &game_logic::on_error }
}, name_(name),throttle_(0.0), rata_(), kaista_(0), pyyntoLahetetty_(false),
kisaAlkanut_(false), edellinenPieceIndex_(0), edellinenInPieceDistance_(0.0),
             nopeus_(0.0), nopeusSaadin_(0.0), turbo_(false), turboKesto_(0.0),
             turboVoima_(0.0)
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
    const auto& msg_type = msg["msgType"].as<std::string>();
    const auto& data = msg["data"];
    auto action_it = action_map.find(msg_type);
    if (action_it != action_map.end())
    {
        return (action_it->second)(this, data);
    }
    else
    {
        std::cout << "Unknown message type: " << msg_type << std::endl;
        return { make_ping() };
    }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
    std::cout << "Joined" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
    std::string name = data["race"]["track"]["id"].as_string();
    std::cout << "Iniating:" << name << std::endl;

    bool vaihto = false;
    double pituus = 0.0;
    double kulma = 0.0;
    double sade = 0.0;

    rata_.asetaNimi(name);
    for(unsigned int i = 0; i < data["race"]["track"]["pieces"].size(); ++i)
    {
        vaihto = false;
        if(data["race"]["track"]["pieces"][i].has_member("switch"))
        {
            vaihto = data["race"]["track"]["pieces"][i]["switch"].as_bool();
        }
        if(data["race"]["track"]["pieces"][i].has_member("length"))
        {
            pituus = data["race"]["track"]["pieces"][i]["length"].as_double();
            rata_.lisaaSuora(pituus, vaihto);
        }
        else
        {
            sade = data["race"]["track"]["pieces"][i]["radius"].as_double();
            kulma = data["race"]["track"]["pieces"][i]["angle"].as_double();
            rata_.lisaaMutka(sade, kulma, vaihto);
        }
    }
    rata_.setKaistoja(data["race"]["track"]["lanes"].size() - 1);


    return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
    std::cout << "No pannaas startaten" << std::endl;
    throttle_ = 0.5;
    kisaAlkanut_ = true;
    return { make_throttle(throttle_) };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
    if(kisaAlkanut_)
    {
        double angle = 90.0;

        unsigned int pieceIndex = 0;
        double inPieceDistance = 0;

        for(unsigned int i = 0; i < data.size(); ++i)
        {
            if(data[i]["id"]["name"] == name_)
            {
                angle = data[i]["angle"].as_double();
                kaista_ = data[i]["piecePosition"]["lane"]["endLaneIndex"].as_int();
                pieceIndex = data[i]["piecePosition"]["pieceIndex"].as_uint();
                inPieceDistance = data[i]["piecePosition"]["inPieceDistance"].as_uint();

            }
        }

        if(pieceIndex == edellinenPieceIndex_)
        {
            nopeus_ = inPieceDistance - edellinenInPieceDistance_;
        }

        double etaisyys = 0.0;
        double haluttuNopeus = rata_.haluttuNopeus(etaisyys, pieceIndex, inPieceDistance);

        double nopeudenMuutos = haluttuNopeus-nopeus_ + nopeusSaadin_;



        if(pieceIndex != edellinenPieceIndex_)
        {
            if(angle > -HIDASKULMA && angle < HIDASKULMA)
            {
                nopeusSaadin_ /= 1.05;
            }//jos kuljetaan liian kovaa ja perä heittelee
            else if (angle < -NOPEAKULMA || angle > NOPEAKULMA)
            {
                nopeusSaadin_ -= 0.25;
            }
            //std::cout << "pieceIndex: " << pieceIndex << std::endl;
            std::cout << "Nyky nopeus:    " << nopeus_ << std::endl;
            std::cout << "Haluttu nopeus: " << haluttuNopeus << std::endl;
            //std::cout << "throttle_: " << throttle_ << std::endl;
            //std::cout << "etaisyys: " << etaisyys << std::endl;
        }
        throttle_ += nopeudenMuutos/5;

        edellinenInPieceDistance_ = inPieceDistance;
        edellinenPieceIndex_ = pieceIndex;

        //tarkastellaan pitäisikö vaihtaa kaistaa
        unsigned int tavoitekaista = rata_.tavoiteKaista(pieceIndex);



        if(pyyntoLahetetty_)
        {
            int siirtymiaNyt = abs(tavoitekaista - kaista_);
            if(siirtymia_ > siirtymiaNyt)
            {
                std::cout << name_ << ": kaista vaihdettu" << std::endl;
                pyyntoLahetetty_ = false;
            }
            siirtymia_ = siirtymiaNyt;
        }
        else
        {
            if(kaista_ < tavoitekaista)
            {
                pyyntoLahetetty_ = true;
                return { make_switch("Right")};
            }
            else if(kaista_ > tavoitekaista)
            {
                pyyntoLahetetty_ = true;
                return { make_switch("Left")};
            }
        }


        //tarkistetaan josko olisi varaa kiihdyttää
        // eli ollaan vielä melko suoraan samasssa kulmassa kuin tie


        if(throttle_ > 0.99)
        {
            throttle_ = 0.99;
        }
        else if(throttle_ < 0.1)
        {
            throttle_ = 0.1;
        }

        if(turbo_ && etaisyys > turboKesto_*turboVoima_/3 && nopeudenMuutos > 12)
        {
            std::cout << "Kaasu pohjaan!" << std::endl;
            turbo_ = false;
            return {make_turbo()};
        }

    }
    // jos ei tarvinnut vaihtaa kaistaa säädetään nopeutta
    return { make_throttle(throttle_) };
}


game_logic::msg_vector game_logic::on_turbo_av(const jsoncons::json& data)
{
    std::cout << name_ << ": TURBO saatavilla!!!" << std::endl;

    turboKesto_ = data["turboDurationMilliseconds"].as_double();
    turboVoima_ = data["turboFactor"].as_double();
    std::cout << "kesto: " << turboKesto_ << std::endl;
    std::cout << "Voima: " << turboVoima_ << std::endl;

    turbo_ = true;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
    std::cout << "Joku lensi metsään!" << std::endl;
    if(data["name"] == name_)
    {
        nopeusSaadin_ -= 1.0;
    }
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
    std::cout << "Se o loppu ny" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
    std::cout << "Error: " << data.to_string() << std::endl;
    return { make_ping() };
}
