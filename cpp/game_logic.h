#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>

#include "rata.hh"


class game_logic
{
public:
  typedef std::vector<jsoncons::json> msg_vector;

  game_logic(std::string name);
  msg_vector react(const jsoncons::json& msg);

private:
  typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
  const std::map<std::string, action_fun> action_map;


  std::string name_;
   // kuinka nopeaa auto kiihdyttää. Välillä 0-1.
   float throttle_;


   Rata rata_;

   unsigned int kaista_;
   int siirtymia_;
   bool pyyntoLahetetty_;

   // onkoo kisa alkanut
   bool kisaAlkanut_;

   unsigned int edellinenPieceIndex_;
   double edellinenInPieceDistance_;

   // millä nopeudella kuljetaan
   double nopeus_;

   // mikäli kurveissa heitellään liikaa, hidastetaan
   double nopeusSaadin_;
   bool turbo_;



   double turboKesto_;
   double turboVoima_;




  msg_vector on_join(const jsoncons::json& data);
  msg_vector on_game_init(const jsoncons::json& data);
  msg_vector on_game_start(const jsoncons::json& data);
  msg_vector on_car_positions(const jsoncons::json& data);
  msg_vector on_turbo_av(const jsoncons::json& data);
  msg_vector on_crash(const jsoncons::json& data);
  msg_vector on_game_end(const jsoncons::json& data);
  msg_vector on_error(const jsoncons::json& data);
};

#endif
